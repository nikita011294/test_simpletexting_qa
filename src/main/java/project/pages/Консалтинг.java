package project.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static project.Base.driver;

public class Консалтинг {


    /**
     * Данный метод заполняет поля в форме "Оставить заявку на консалтинг"
     */
    public Консалтинг оставить_заявку(String фио, String телефон, String почта) {
        оставить_заявку(фио, телефон);

        WebElement поле_почта = driver.findElement
                (By.xpath("//div [@class='form-section']//input [@placeholder ='Контактный e-mail']"));

        поле_почта.sendKeys(почта);
        return new Консалтинг();
    }

    public Консалтинг заполнить_компания_сообщенеие(String сompany, String messege) {

        WebElement поле_company = driver.findElement
                (By.xpath("//div [@class='form-section']//input [@placeholder ='Компания']"));
        WebElement поле_messege = driver.findElement
                (By.xpath("//div [@class='form-section']//textarea [@placeholder ='Сообщение']"));

        поле_company.sendKeys(сompany);
        поле_messege.sendKeys(messege);


        return new Консалтинг();
    }

    /**
     * Данный метод заполняет поля в форме "ФИО телефон"
     */
    public Консалтинг оставить_заявку(String фио, String телефон) {

        WebElement поле_ФИО = driver.findElement
                (By.xpath("//div [@class='form-section']//input [@placeholder ='Фамилия, имя, отчество']"));
        WebElement поле_контактный_телефон = driver.findElement
                (By.xpath("//div [@class='form-section']//input [@placeholder ='Контактный телефон']"));
        поле_ФИО.sendKeys(фио);
        поле_контактный_телефон.sendKeys(телефон);


        return new Консалтинг();
    }

}
