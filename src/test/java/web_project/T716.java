package web_project;

import project.pages.MainPage;
import project.pages.Расписание_и_цены;

import static automationTesting.allureReport.Steps.result;
import static automationTesting.allureReport.Steps.step;
import static project.pages.MainPage.Вкладки.Расписание;

public class T716 {

    private MainPage mainPage;
    private Расписание_и_цены расписание_и_цены;

    String[] кнопки = {"Расписание", "Бесплатные семинары", "Онлайн-курсы"};

    public void scenario_T716() {

        step("[Step 1]: Перейти на вкладку 'Расписание...'", () -> {
            mainPage = new MainPage();
            расписание_и_цены = mainPage
                    .userGoToPage(Расписание)
                    .return_to_page(new Расписание_и_цены());

            result("Перешли на вкладку «Расписание»");
        });

    }
}
