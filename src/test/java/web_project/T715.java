package web_project;

import project.pages.MainPage;
import project.pages.Расписание_и_цены;

import java.util.Arrays;

import static automationTesting.allureReport.Steps.result;
import static automationTesting.allureReport.Steps.step;
import static project.pages.MainPage.Вкладки.Расписание;

public class T715 {
    private MainPage mainPage;
    private Расписание_и_цены расписание_и_цены;

    String[] кнопи = {"Расписание", "Бесплатные семинары", "Онлайн-курсы"};

    public void scenario_T715() {

        step("[Step 1]: Перейти на вкладку 'Расписание'", () -> {
            mainPage = new MainPage();
            расписание_и_цены = mainPage
                    .userGoToPage(Расписание)
                    .return_to_page(new Расписание_и_цены());

            result("Перешли на вкладку «Расписание»");
        });

        step("[Step 2]: Проверить отображение кнопок 'Расписание', 'Бесплатные семинары' и 'Онлайн-курсы'", () -> {
            расписание_и_цены.check_buttons_exist(Arrays.asList(кнопи));

            result("Кнопки 'Расписание', 'Бесплатные семинары' и 'Онлайн-курсы' отображены на странице");
        });

        step("[Step 3]: Проверить отображение кнопок 'Расписание'", () -> {
            расписание_и_цены.check_buttons_exist(Arrays.asList(кнопи));

            result("Кнопки 'Расписание' отображены на странице");
        });

    }
}
