package web_project;

import project.pages.MainPage;
import project.pages.Консалтинг;

import static automationTesting.allureReport.Steps.result;
import static automationTesting.allureReport.Steps.step;

public class T1292 {

    private Консалтинг открыть_консалтинг;
    private Консалтинг консалтинг;

    public void scenario_T1292() {
        step("[Step 1]: Перейти на вкладку 'Конслтинг'", () -> {
            консалтинг = new Консалтинг();
            открыть_консалтинг = MainPage.getInstance().открыть_консалтинг();
            result("Перешли на вкладку 'Конслтинг'");
        });

        step("[Step 2]: Оставить заявку на консалтинг", () -> {
            консалтинг.оставить_заявку("Ошурков Ниикта Юрьевич", "8374837877387", "nik@mail");
            result("Оставить заявку на консалтинг");
        });

        step("[Step 3]: Оставить заявку на консалтинг компании и сообщение", () -> {
            консалтинг.заполнить_компания_сообщенеие( "nik@mail","nikcompany");
            result("Оставить заявку на консалтинг компании и сообщение");
        });

        step("[Step 4]: заполняет поля в форме ФИО телефон", () -> {
            консалтинг.заполнить_компания_сообщенеие( "luxsot","hello my company");
            result("Оставить заявку на консалтинг компании и сообщение");
        });
    }
}
